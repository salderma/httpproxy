# class httpproxy::gui
#
# @summary configure proxy settings in the linux gui
#
# @api private
#
class httpproxy::gui {

  assert_private('This class is a private api, do not call it directly.')

  case $::osfamily {
    'RedHat': {
      contain httpproxy::gui::dconf
    }
    default: { fail("This Linux Distribution ${facts['os']['name']} is not supported!!") }
  }

}
