# httpproxy::gui::dconf
#
# @summary configure gnome's proxy settings
#
# @api private
#
class httpproxy::gui::dconf {

  assert_private('This class is a private api, do not call it directly.')

  $gui_file_path = '/etc/dconf/db/local.d/10_proxy'
  $gui_lock_path = '/etc/dconf/db/local.d/locks/10_proxy'
  $gui_enable = $httpproxy::enable_gui
  $gui_ensure = $gui_enable ? {
    true    => 'present',
    default => 'absent',
  }

  # ensure the dconf files exist with proper mode/ownership
  file { $gui_file_path:
    ensure  => $gui_ensure,
    mode    => '0640',
    owner   => 'root',
    group   => 'root',
    content => template('httpproxy/dconf_proxy.erb'),
    notify  => Exec['refresh-dconf'],
  }

  file { $gui_lock_path:
    ensure  => $gui_ensure,
    mode    => '0640',
    owner   => 'root',
    group   => 'root',
    content => template('httpproxy/dconf_proxy_locks.erb'),
    notify  => Exec['refresh-dconf'],
  }

  exec { 'refresh-dconf':
    command     => '/bin/dconf update',
    umask       => '022',
    refreshonly => true,
  }

}
