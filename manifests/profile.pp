# class httpproxy::profile
#
# @summary configures proxy settings for system shells
#
# @api private
class httpproxy::profile {

  assert_private('This class is a private api, do not call it directly.')

  $profile_path   = '/etc/profile.d'
  $profile_enable = $httpproxy::enable_profile
  $profile_ensure = $httpproxy::enable_profile ? {
    true    => 'present',
    default => 'absent',
  }

  file { $profile_path:
    ensure =>  directory,
  }

  file { "${profile_path}/proxy.sh":
      ensure  => $profile_ensure,
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('httpproxy/proxy.sh.erb'),
      require => File[$profile_path],
  }

}
