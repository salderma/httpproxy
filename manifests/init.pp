# @summary A puppet module to configure proxy settings for web clients on a Linux system
#
# @example
#   class { 'httpproxy':
#     proxy_host     => 'https://proxy.example.com',
#     proxy_port     => '3128',
#     no_proxy       => [ '.example.com', '192.168.*.*'],
#     enable_profile => true,
#     enable_pkgmgr  => true,
#     enable_gui     => true,
#   }
#
# @see https://www.w3.org/Daemon/User/Proxies/ProxyClients.html
#
# @param proxy_host
#   String containing proxy URL
# @param proxy_port
#   String containing proxy port
# @param no_proxy
#   Array of Strings containing sites excluded from proxying
# @param enable_profile
#   Boolean to enable or disable setting proxy variables in the shell environment
# @param enable_pkgmgr
#   Boolean to enable or disable setting proxy for the system's package manager
# @param enable_gui
#   Boolean to enable to disable setting proxy for the system's gui interface, e.g. Gnome
#
class httpproxy (
  String            $proxy_host,
  String            $proxy_port,
  Array             $no_proxy,
  Optional[Boolean] $enable_profile,
  Optional[Boolean] $enable_pkgmgr,
  Optional[Boolean] $enable_gui,
) {

  contain httpproxy::profile
  contain httpproxy::pkgmgr
  contain httpproxy::gui

}
