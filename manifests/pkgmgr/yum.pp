# class httpproxy::pkgmgr::yum
#
# @summary configures yum to use the proxy
#
# @api private
#
class httpproxy::pkgmgr::yum {

  assert_private('This class is a private api, do not call it directly.')

  $pkgmgr_ensure = $httpproxy::enable_pkgmgr ? {
    true    => 'present',
    default => 'absent',
  }

  ini_setting { 'yum_proxy':
    ensure  => $pkgmgr_ensure,
    path    => '/etc/yum.conf',
    section => 'main',
    setting => 'proxy',
    value   => "http://${httpproxy::proxy_host}:${httpproxy::proxy_port}",
  }

}
