# class httpproxy::pkgmgr::tpm
#
# @summary configures rpm to utilize proxy setting
#
# @api private
#
class httpproxy::pkgmgr::rpm {

  assert_private('This class is a private api, do not call it directly.')

  $pkgmgr_ensure = $httpproxy::enable_pkgmgr ? {
    true    => 'present',
    default => 'absent',
  }

  file { '/etc/rpm/macros.httpproxy':
    ensure  => $pkgmgr_ensure,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('httpproxy/rpm.macros.erb'),
  }

}
