# class httpproxy::pkgmgr
#
# @summary configures system package management to utilize the proxy
#
# @api private
#
class httpproxy::pkgmgr {

  assert_private('This class is a private api, do not call it directly.')

  $pkgmgr_enable = $httpproxy::enable_pkgmgr
  $pkgmgr_ensure = $pkgmgr_enable ? {
    true    => 'present',
    default => 'absent',
  }

  case $::osfamily {
    'RedHat': {
      contain httpproxy::pkgmgr::rpm
      contain httpproxy::pkgmgr::yum
    }
    default: { fail("This module does not support ${facts['os']['name']} (yet).") }
  }

}
