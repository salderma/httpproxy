require 'spec_helper'

describe 'httpproxy' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_class('httpproxy::profile') }
      it { is_expected.to contain_class('httpproxy::pkgmgr') }

      describe 'with gui enabled' do
        let(:params) { { enable_gui: true } }

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('httpproxy::gui') }
      end
    end
  end
end
